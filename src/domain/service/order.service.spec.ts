import Customer from "../entity/customer";
import Order from "../entity/order";
import OrderItem from "../entity/order_item";
import OrderService from "./order.service";

describe("OrderService unit tests:",() => {
    it("Should get the sum of the total of a array of orders", () => {
        const o1it1 = new OrderItem("111","it1", 100,"p1",1);
        const o2it1 = new OrderItem("112","it2", 200,"p2",2);
        const o1 = new Order("o1","c1",[o1it1]);
        const o2 = new Order("o2","c1",[o2it1]);

        let total = OrderService.total([o1,o2]);
        
        expect(total).toBe(500);
    })
    it("Should place an order", () => {
        const customer1 = new Customer("222","nome custom 2");
        const orderItem1 = new OrderItem("oi222","name222",10,"p1",1);
        const order = OrderService.placeOrder(customer1,[orderItem1]);
        expect(customer1.rewardPoints).toBe(5);
        expect(order.total()).toBe(10);
    })
})