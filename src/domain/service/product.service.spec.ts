import Product from "../entity/product";
import ProductService from "./product.service";

describe("ProductService unit tests:",() => {
    it("Should change the price of a array of products", () => {
        const product1 = new Product("111","ppp1", 10);
        let product2 = new Product("222","ppp2", 20);
        const products = [product1,product2];
        ProductService.increasePrice(products, 100);
        expect(product1.price).toBe(20);
    })
})