import Address from "./address";
import Customer from "./customer";

describe("Customer unit tests:",() => {
    it("Should throw error when Id has lenght equal zero", () => {
        expect(() => {
            let customer = new Customer("", "custom 1")
        }).toThrowError("Id cannot have length equal zero");
    })

    it("Should throw error when name has lenght equal zero", () => {
        expect(() => {
            let customer = new Customer("111", "")
        }).toThrowError("Name cannot have length equal zero");
    })

    it("Should throw error when try to change name to a zero length name", () => {
        expect(() => {
            let customer = new Customer("111", "nome 1");
            customer.changeName("");
        }).toThrowError("changeName method does not accepts a zero length name parameter");
    })

    it("Should change name to a non-zero length name", () => {
        let customer = new Customer("111", "nome 1");
        customer.changeName("name 2");
        expect(customer.name).toBe("name 2");
    })

    it("Should activate customer", () => {
        let customer = new Customer("111", "nome 1");
        let addr = new Address("aaaa","bbbb","cccc","dddd");
        customer.Address = addr;
        customer.activate();
        expect(customer.isActive()).toBe(true);
    }) 

    it("Should not activate customer withouth address", () => {
        let customer = new Customer("111", "nome 1");
        expect(() => {
            customer.activate()
        }).toThrowError("Address is mandatory to activate a customer");
    })    

    it("Should deactivate customer", () => {
        let customer = new Customer("111", "nome 1");
        let addr = new Address("aaaa","bbbb","cccc","dddd");
        customer.Address = addr;
        customer.activate();
        customer.deactivate();
        expect(customer.isActive()).toBe(false);
    }) 

    it("Should add rewards points", () => {
        let customer = new Customer("111", "nome 1");
        expect(customer.rewardPoints).toBe(0);

        customer.addRewardsPoints(10);
        expect(customer.rewardPoints).toBe(10);

        customer.addRewardsPoints(10);
        expect(customer.rewardPoints).toBe(20);
    }) 
})