import OrderItem from "./order_item"

export default class Order{
    private _id: string;
    private _customerId: string;
    private _items: OrderItem[] = [];
    private _total: number;

    constructor(
        id: string,
        customerId: string,
        items: OrderItem[]
    ){
        this._id = id;
        this._customerId = customerId;
        this._items = items;
        this._total = this.total();
        this.validate();
    }

    get id(): string{
        return this._id;
    }
    get customerId(): string{
        return this._customerId;
    }
    get items(): OrderItem[]{
        return this._items;
    }

    validate(): boolean{
        if(this._id.length === 0){
            throw new Error("Id cannot have length iqual zero");
        }
        if(this._customerId.length === 0){
            throw new Error("CustomerId cannot have length iqual zero");
        }
        if(this._items.length === 0){
            throw new Error("Items cannot have length iqual zero");
        }
        if(this._items.some(item => item.quantity <= 0)){
            throw new Error("Item quantity must be greater than zero");
        }
        return true;
    }

    total(): number{
        return this._items.reduce((acc,item) => acc + item.orderItemTotal(), 0);
    }

    changeCustomer(customerId: string): void{
        this._customerId = customerId;
    }

    changeItems(newItems: OrderItem[]): void{
        this._items = newItems;
    }
}