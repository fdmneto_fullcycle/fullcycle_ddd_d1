import Order from "./order";
import orderItem from "./order_item";

describe("Order unit tests:",() => {
    let item1 = new orderItem("it1","Item 1",10,"aaa",1);
    let item2 = new orderItem("it2","Item 2",20,"bbb",2);
    let items = [item1, item2];
    it("Should throw error when Id has lenght equal zero", () => {
        expect(() => {
            let order = new Order("", "custom_id1",items)
        }).toThrowError("Id cannot have length iqual zero");
    })

    it("Should calculate total from itens", () => {
        let order = new Order("order_id_1", "custom_id1",items)
        expect(order.total()).toBe(50);
    })

    it("Should throw error when CustomerId has lenght equal zero", () => {
        expect(() => {
            items = [];
            let order = new Order("order_1", "custom_id1",items)
        }).toThrowError("Items cannot have length iqual zero");
    })
})