import Product from "./product";

describe("Product unit tests:",() => {
    it("Should throw error when Id has lenght equal zero", () => {
        expect(() => {
            let product = new Product("", "nome_prod1",10)
        }).toThrowError("Id cannot have length iqual zero");
    })
    it("Should throw error when Name has lenght equal zero", () => {
        expect(() => {
            let product = new Product("id1", "",10)
        }).toThrowError("Name cannot have length iqual zero");
    })
    it("Should throw error when Price was lesser than zero", () => {
        expect(() => {
            let product = new Product("id1", "name prod",-10)
        }).toThrowError("Price cannot be less than zero");
    })
    it("Should change Name", () => {
        let product = new Product("id1", "name prod",10)
        product.changeName("bla");
        expect(product.name).toBe("bla");
    })
    it("Should change Price", () => {
        let product = new Product("id1", "name prod",10)
        product.changePrice(20);
        expect(product.price).toBe(20);
    })
})