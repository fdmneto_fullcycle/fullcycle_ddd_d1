import OrderItem from "./order_item";

describe("Order_item unit tests:",() => {
    it("Should throw error when Id has lenght equal zero", () => {
        expect(() => {
            let orderItem = new OrderItem("", "prodId",10,"nomeItem",1)
        }).toThrowError("Id cannot have length equal zero");
    })
    it("Should throw error when ProductId has lenght equal zero", () => {
        expect(() => {
            let orderItem = new OrderItem("111", "",10,"nomeItem",1)
        }).toThrowError("Name cannot have length equal zero");
    })
    it("Should throw error when Name has length equal zero", () => {
        expect(() => {
            let orderItem = new OrderItem("111", "11111",10,"",1)
        }).toThrowError("ProductId cannot have length equal zero");
    })
    it("Should throw error when Quantity is lesser than zero", () => {
        expect(() => {
            let orderItem = new OrderItem("111", "11111",10,"aaa",-1)
        }).toThrowError("Quantity cannot be less than or equal zero");
    })
    it("Should throw error when Quantity is lesser than zero", () => {
        expect(() => {
            let orderItem = new OrderItem("111", "11111",10,"aaa",0)
        }).toThrowError("Quantity cannot be less than or equal zero");
    })
    it("Should throw error when Price is lesser than zero", () => {
        expect(() => {
            let orderItem = new OrderItem("111", "11111",-10,"bbb",1)
        }).toThrowError("Price cannot be lesser than zero");
    })
})