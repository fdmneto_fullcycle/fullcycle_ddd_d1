export default class OrderItem{
    private _id: string;
    private _productId: string = "";
    private _name: string = "";
    private _price: number;
    private _quantity: number;

    constructor(id: string, name: string, price: number, productId: string, quantity: number){
        this._id = id;
        this._productId = productId;
        this._name = name;
        this._price = price;
        this._quantity = quantity;
        this.validate();
    }

    validate(){
        if(this._id.length === 0){
            throw new Error("Id cannot have length equal zero");
        }
        if(this._productId.length === 0){
            throw new Error("ProductId cannot have length equal zero");
        }
        if(this._name.length === 0){
            throw new Error("Name cannot have length equal zero");
        }
        if(this._price === undefined){
            throw new Error("Price não foi definido");
        }
        if(this._price < 0){
            throw new Error("Price cannot be lesser than zero");
        }
        if(this._quantity === undefined){
            throw new Error("Quantity não foi definido");
        }
        if(this._quantity <= 0){
            throw new Error("Quantity cannot be less than or equal zero");
        }
    }

    get id(){
        return this._id;
    }
    get productId(): string{
        return this._productId;
    }
    get name(){
        return this._name;
    }
    get price(){
        return this._price;
    }
    get quantity(){
        return this._quantity;
    }

    orderItemTotal(): number{
        return this._price * this._quantity;
    }
}