export default class Address{
    private _street: string;
    private _number: string;
    private _city: string;
    private _zip: string;
    private _active: boolean = false;

    constructor (
        _street: string,
        _number: string,
        _city: string,
        _zip: string,        
        ){
        this._street = _street;
        this._number = _number;
        this._city = _city;
        this._zip = _zip;
        this.validate();
    }

    get street(){
        return this._street;
    }
    get number(){
        return this._number;
    }
    get city(){
        return this._city;
    }
    get zip(){
        return this._zip;
    }
    get active(){
        return this._active;
    }
    
    validate(){
        if(this._street.length === 0){
            throw new Error("Street may not have length equal zero")
        };
        if(this._number.length === 0){
            throw new Error("Number may not have length equal zero")
        }
        if(this._city.length === 0){
            throw new Error("City may not have length equal zero")
        }
        if(this._zip.length === 0){
            throw new Error("Zip may not have length equal zero")
        }
    }

    toStrig(){
        return `${this._street}, ${this._number},${this._city},${this._zip}`
    }
}