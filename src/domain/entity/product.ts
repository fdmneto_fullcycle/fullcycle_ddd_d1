export default class Product{
    private _id: string;
    private _name: string;
    private _price: number;


    constructor(id: string, name: string, price: number){
        this._id = id;
        this._name = name;
        this._price = price;
        this.validate();
    }

    get id():string{
        return this._id;
    }
    get name():string{
        return this._name;
    }    
    get price():number{
        return this._price;
    }  
    
    
    validate(): boolean{
        if(this._id.length === 0){
            throw new Error("Id cannot have length iqual zero");
        }
        if(this._name.length === 0){
            throw new Error("Name cannot have length iqual zero");
        }
        if(this._price === undefined){
            throw new Error("Price is necessary");
        }
        if(this._price < 0){
            throw new Error("Price cannot be less than zero");
        }
        return true;
    }

    changeName(newName: string):void{
        this._name = newName;
        this.validate();
    }

    changePrice(newPrice: number): void{
        this._price = newPrice;
        this.validate();
    }
}