import Order from "../../domain/entity/order";
import OrderRepositoryInterface from "../../domain/repository/order-repository.interface";
import OrderItemModel from "../db/sequelize/model/order-item.model";
import OrderItem from '../../domain/entity/order_item';
import OrderModel from "../db/sequelize/model/order.model";
import {v4 as uuid} from 'uuid';

export default class OrderRepository implements OrderRepositoryInterface{
    async create(entity: Order): Promise<void>{
        await OrderModel.create(
            {
                id: entity.id,
                customer_id: entity.customerId,
                total: entity.total(),
                items: entity.items.map((item) => ({
                    id: item.id,
                    name: item.name,
                    price: item.price,
                    product_id: item.productId,
                    quantity: item.quantity,
                }))
            },
            {
                include: [{model: OrderItemModel}]
            }
        )
    };

    async update(entity: Order): Promise<void>{
        await OrderItemModel.destroy(
            {
                where:{order_id: entity.id},
            }
        )
        
        entity.items.forEach(async item => {
            await OrderItemModel.create({
                id: item.id,
                product_id: item.productId,
                order_id: entity.id,
                name: item.name,
                price: item.price,
                quantity: item.quantity
            });
        });

        const newItems = entity.items.map((item) => ({
            id: item.id,
            name: item.name,
            price: item.price,
            product_id: item.productId,
            quantity: item.quantity,
        }));

        await OrderModel.update(
            {
                customer_id: entity.customerId,
                total: entity.total()
            },
            {
                where:{id: entity.id},
            },
        )
    }

    async find(id: string): Promise<Order> {
        let orderModel;
        try{
            orderModel =  await OrderModel.findOne(
                {
                    where: {id: id},
                    rejectOnEmpty: true,
                    include: [
                        {
                            model: OrderItemModel,
                            as: 'items'
                        }
                    ]
                },
                
            );
        }
        catch{
            throw new Error("Order not found");
        }

        let items = orderModel.items.map((item) => new OrderItem(
            item.id,
            item.name,
            item.price,
            item.product_id,
            item.quantity
        ))
        const order = new Order(orderModel.id,orderModel.customer_id,items);
        return order;
    }



    async findAll(): Promise<Order[]>{
        const allOrdersModel = await OrderModel.findAll({
            include: {
                model: OrderItemModel, 
                as: "items"
            },
        });
        const orders =  allOrdersModel.map((orderModel) => {
            let items = orderModel.items.map((item) => new OrderItem(
                item.id,
                item.name,
                item.price,
                item.product_id,
                item.quantity
            ))

            const order = new Order(orderModel.id, orderModel.customer_id, items)
            return order;
        });
        return orders;
    };
}