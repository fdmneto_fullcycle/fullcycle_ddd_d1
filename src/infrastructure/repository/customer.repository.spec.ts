import {Sequelize} from 'sequelize-typescript';
import CustomerModel from '../db/sequelize/model/customer.model';
import Address from '../../domain/entity/address';
import Customer from '../../domain/entity/customer';
import CustomerRepository from './customer.repository';
import {v4 as uuid} from 'uuid';

describe("Customer repository tests:", () => {
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: {force: true}
        });
        sequelize.addModels([CustomerModel]);
        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it("Should create a customer", async () => {
        const addr = new Address("rua1","111","cidade1","zip1");
        const customer = new Customer(uuid(), "Customer 1",);
        customer.changeAddress(addr);

        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer);

        const customerModel = await CustomerModel.findOne({where: {id: customer.id}});
        expect(customerModel.toJSON()).toStrictEqual({
            id: customer.id,
            name: customer.name,
            active: customer.isActive(),
            rewardPoints: customer.rewardPoints,
            street: addr.street,
            number: addr.number,
            city: addr.city,
            zip: addr.zip,
        });
    });

    it("Should update a customer", async () => {
        const custom = new Customer(uuid(), "Custom 2");
        const addr = new Address("rua 2","222","cidade 2","zip2");
        custom.changeAddress(addr);

        const customerRepository = new CustomerRepository();
        await customerRepository.create(custom);
        
        custom.changeName("Custom 222");
        await customerRepository.update(custom);

        const customerFound = await CustomerModel.findOne({where: {id: custom.id}});
        expect(customerFound.toJSON()).toStrictEqual({
            id: custom.id,
            name: custom.name,
            active: custom.isActive(),
            rewardPoints: custom.rewardPoints,
            street: addr.street,
            number: addr.number,
            city: addr.city,
            zip: addr.zip,
        });
    })

    it("Should find a customer", async () => {
        const custom = new Customer(uuid(), "Custom 2");
        const addr = new Address("rua 2","222","cidade 2","zip2");
        custom.changeAddress(addr);
        custom.addRewardsPoints(5);
        custom.activate();

        const customerRepository = new CustomerRepository();
        await customerRepository.create(custom);

        const customFound = await customerRepository.find(custom.id);
        expect(customFound).toStrictEqual(custom);
    })

    it("Should throw an error when not find an customer", async () => {
        const customerRepository = new CustomerRepository();

        expect(async () => {
          await customerRepository.find("bla");
        }).rejects.toThrow("Customer not found");
    })

    it("Should find all customer", async () => {
        const customerRepository = new CustomerRepository();

        const custom1 = new Customer(uuid(), "Custom 1");
        const addr1 = new Address("rua 1","111","cidade 1","zip1");
        custom1.changeAddress(addr1);
        custom1.addRewardsPoints(5);
        custom1.activate();
        await customerRepository.create(custom1);

        const custom2 = new Customer(uuid(), "Custom 2");
        const addr2 = new Address("rua 2","222","cidade 2","zip2");
        custom2.changeAddress(addr2);
        custom2.addRewardsPoints(55);
        custom2.activate();
        await customerRepository.create(custom2);

        const customersFound = await customerRepository.findAll();
        const customersCreated = [custom1,custom2]
        
        expect(customersFound).toEqual(customersCreated);
    })
})