import Address from "../../domain/entity/address";
import Customer from "../../domain/entity/customer";
import CustomerRepositoryInterface from "../../domain/repository/customer-repository.interface";
import CustomerModel from "../db/sequelize/model/customer.model";

export default class CustomerRepository implements CustomerRepositoryInterface{
    async create(entity: Customer): Promise<void>{
        await CustomerModel.create({
            id: entity.id,
            name: entity.name,
            street: entity.Address.street,
            number: entity.Address.number,
            city: entity.Address.city,
            zip: entity.Address.zip,
            active: entity.isActive(),
            rewardPoints: entity.rewardPoints
        })
    };

    async update(entity: Customer): Promise<void>{
        await CustomerModel.update(
            {
                name: entity.name,
                street: entity.Address.street,
                number: entity.Address.number,
                city: entity.Address.city,
                zip: entity.Address.zip,
                active: entity.isActive(),
                rewardPoints: entity.rewardPoints
            },
            {
                where:{id: entity.id}
            }
        );
    }
    
    async find(id: string): Promise<Customer> {
        let customModel;
        try{
            customModel =  await CustomerModel.findOne({
                where: {id: id},
                rejectOnEmpty: true
            });
        }
        catch{
            throw new Error("Customer not found");
        }
        const addr = new Address(customModel.street,customModel.number,customModel.city, customModel.zip);
        const customer = new Customer(customModel.id, customModel.name);
        customer.changeAddress(addr);
        customer.addRewardsPoints(customModel.rewardPoints);
        if(customModel.active){
            customer.activate();
        } else {
            customer.deactivate();
        }

        return customer;
    };

    async findAll(): Promise<Customer[]>{
        const allCustomersModel = await CustomerModel.findAll();
        const customers =  allCustomersModel.map((customerModel) => {
            const addr = new Address(customerModel.street,customerModel.number,customerModel.city, customerModel.zip);
            const customer = new Customer(customerModel.id, customerModel.name);
            customer.changeAddress(addr);
            customer.addRewardsPoints(customerModel.rewardPoints);
            if(customerModel.active){
                customer.activate();
            } else {
                customer.deactivate();
            }
            return customer;
        });
        return customers;
    };
}