import {Sequelize} from 'sequelize-typescript';
import OrderItemModel from '../db/sequelize/model/order-item.model';
import OrderItem from '../../domain/entity/order_item';
import OrderModel from '../db/sequelize/model/order.model';
import Order from '../../domain/entity/order';
import OrderRepository from './order.repository';
import CustomerModel from '../db/sequelize/model/customer.model';
import ProductModel from '../db/sequelize/model/product.model';
import ProductRepository from './product.repository';
import Product from '../../domain/entity/product';
import CustomerRepository from './customer.repository';
import Customer from '../../domain/entity/customer';
import Address from '../../domain/entity/address';
import {v4 as uuid} from 'uuid';

describe("Order repository tests:",() => {
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: {force: true}
        });

        sequelize.addModels([CustomerModel,OrderModel,OrderItemModel,ProductModel]);
        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it("Should create an order", async () => {
        const addr = new Address("rua1","111","cidade1","zip1");
        const customer = new Customer(uuid(), "Customer 1",);
        customer.changeAddress(addr);
        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product(uuid(), "Product 1", 10);
        await productRepository.create(product);

        const orderItem = new OrderItem(
            uuid(),
            product.name,
            product.price,
            product.id,
            2
        );

        const order = new Order(uuid(), customer.id, [orderItem]);
        const orderRepository = new OrderRepository();
        await orderRepository.create(order);

        const orderFound = await OrderModel.findOne({
            where: {id: order.id},
            include: ["items"]
        })
        expect(orderFound.toJSON()).toStrictEqual({
            id: order.id,
            customer_id: customer.id,
            total: order.total(),
            items: [
                {
                    id: orderItem.id,
                    name: orderItem.name,
                    price: orderItem.price,
                    quantity: orderItem.quantity,
                    product_id: orderItem.productId,
                    order_id: order.id
                }
            ],
        });
    });


    it("Should update an order", async () => {
        const addr = new Address("ruaUpdate1","Update111","cidadeUpdate1","zipUpdate1");
        const customer = new Customer(uuid(), "CustomerUpdate 1",);
        customer.changeAddress(addr);
        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product(uuid(), "ProductUpdate 1", 10);
        await productRepository.create(product);

        const orderItem = new OrderItem(
            uuid(),
            product.name,
            product.price,
            product.id,
            2
        );

        const order = new Order(uuid(), customer.id, [orderItem]);
        const orderRepository = new OrderRepository();
        await orderRepository.create(order);

        const addrChanged = new Address("ruaChanged1","Changed111","cidadeChanged1","zipChanged1");
        const customerChanged = new Customer(uuid(), "CustomerChanged 1",);
        customerChanged.changeAddress(addrChanged);
        await customerRepository.create(customerChanged);

        const productChanged = new Product(uuid(), "ProductChanged 1", 20);
        await productRepository.create(productChanged);

        const orderItemChanged = new OrderItem(
            uuid(),
            productChanged.name,
            productChanged.price,
            productChanged.id,
            1
        );

        order.changeCustomer(customerChanged.id); 
        order.changeItems([orderItemChanged]);

        await orderRepository.update(order);

        const orderFound = await OrderModel.findOne({
            where: {id: order.id},
            include: ["items"]
        })

        expect(orderFound.toJSON()).toStrictEqual({
            id: order.id,
            customer_id: customerChanged.id,
            total: order.total(),
            items: [
                {
                    id: orderItemChanged.id,
                    name: orderItemChanged.name,
                    price: orderItemChanged.price,
                    quantity: orderItemChanged.quantity,
                    product_id: orderItemChanged.productId,
                    order_id: order.id
                }
            ],
        });
    });

    it("Should find an order", async () => {
        const addr = new Address("ruaFind1","Find1","cidadeFind1","zipFind1");
        const customer = new Customer(uuid(), "CustomerFind 1",);
        customer.changeAddress(addr);
        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product1 = new Product(uuid(), "ProductFind 1", 10);
        await productRepository.create(product1);
        const product2 = new Product(uuid(), "ProductFind 2", 20);
        await productRepository.create(product2);

        const orderItem1 = new OrderItem(
            uuid(),
            product1.name,
            product1.price,
            product1.id,
            1
        );
        const orderItem2 = new OrderItem(
            uuid(),
            product2.name,
            product2.price,
            product2.id,
            1
        );

        const order = new Order(uuid(), customer.id, [orderItem1, orderItem2]);
        const orderRepository = new OrderRepository();
        await orderRepository.create(order);

        const orderFound = await orderRepository.find(order.id);

        expect(order).toStrictEqual(orderFound);
    });

    it("Should not find an order", async () => {
        const orderRepository = new OrderRepository();
        expect(async () => {
            await orderRepository.find("bla")
        }).rejects.toThrowError("Order not found");
    });

    it("Should not find all orders", async () => {
        const addr1 = new Address("rua1","1","cidade1","zip1");
        const customer1 = new Customer(uuid(), "Customer 1",);
        customer1.changeAddress(addr1);
        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer1);

        const productRepository = new ProductRepository();
        const product1 = new Product(uuid(), "Product 1", 10);
        await productRepository.create(product1);

        const orderItem1 = new OrderItem(
            uuid(),
            product1.name,
            product1.price,
            product1.id,
            2
        );

        const order1 = new Order(uuid(), customer1.id, [orderItem1]);
        const orderRepository = new OrderRepository();
        await orderRepository.create(order1);

        const addr2 = new Address("rua2","2","cidade2","zip2");
        const customer2 = new Customer(uuid(), "Customer 2",);
        customer2.changeAddress(addr2);
        await customerRepository.create(customer2);

        const product2 = new Product(uuid(), "Product 2", 20);
        await productRepository.create(product2);

        const orderItem2 = new OrderItem(
            uuid(),
            product2.name,
            product2.price,
            product2.id,
            4
        );

        const order2 = new Order(uuid(), customer2.id, [orderItem2]);
        await orderRepository.create(order2);

        const ordersFound = await orderRepository.findAll();

        expect(ordersFound).toEqual([order1,order2]);
    });
})