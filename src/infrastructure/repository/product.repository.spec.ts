import {Sequelize} from 'sequelize-typescript';
import ProductModel from '../db/sequelize/model/product.model';
import Product from '../../domain/entity/product';
import ProductRepository from './product.repository';
import {v4 as uuid} from 'uuid';

describe("Product repository tests:",() => {
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: {force: true}
        });
        sequelize.addModels([ProductModel]);
        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it("Should create a product", async () => {
        const productRepository = new ProductRepository();
        const product = new Product(uuid(), "Product 1",100);
        await productRepository.create(product);
        
        const productModel = await ProductModel.findOne({where: {id: product.id}});
        expect(productModel.toJSON()).toStrictEqual({
            id: product.id,
            name: "Product 1",
            price: 100,
        });
    })

    it("Should update a product", async () => {
        const productRepository = new ProductRepository();
        const product = new Product("2", "Product 2",200);
        await productRepository.create(product);

        const productModel = await ProductModel.findOne({where: {id: "2"}});
        expect(productModel.toJSON()).toStrictEqual({
            id: "2",
            name: "Product 2",
            price: 200,
        });
        product.changeName("Produto 22");
        product.changePrice(222);
        await productRepository.update(product);

        const productModel2 = await ProductModel.findOne({where: {id: "2"}});
        expect(productModel2.toJSON()).toStrictEqual({
            id: "2",
            name: "Produto 22",
            price: 222,
        });
    })
    it("Should find a product", async () => {
        const productRepository = new ProductRepository();

        const product = new Product("3", "Product 3",300);
        await productRepository.create(product);

        const productModel = await ProductModel.findOne({where: {id: "3"}});
        const foundProd = await productRepository.find("3");

        expect(productModel.toJSON()).toStrictEqual({
            id: foundProd.id,
            name: foundProd.name,
            price: foundProd.price,
        });
    })

    it("Should find all products", async () => {
        const productRepository = new ProductRepository();
        const product = new Product("10", "Product 10",1000);
        await productRepository.create(product);
        const product2 = new Product("20", "Product 20",2000);
        await productRepository.create(product2);

        const foundProducts = await productRepository.findAll();
        const createdProducts = [product,product2]
        
        expect(createdProducts).toEqual(foundProducts);
    })
})